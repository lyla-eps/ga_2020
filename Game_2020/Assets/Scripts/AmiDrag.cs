﻿using UnityEngine;

public class AmiDrag : MonoBehaviour
{
    GameObject Left_Arm, Right_Arm;
    Rigidbody isRigidbody;
    public Vector3 offset;
    Vector3 TapPos;
    Vector3 Re_LeftArm, Re_RightArm;
    static public bool dropFrag;
    void Start()
    {
        Left_Arm = GameObject.Find("sm_ch_player_l");
        Right_Arm = GameObject.Find("sm_ch_player_r");
        isRigidbody = gameObject.GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (GameObject.Find("GameManager") != null)
        {
            TyutorialManager tyutorialManager = GameObject.Find("GameManager").GetComponent<TyutorialManager>();
            //tyutorialManager.DishMoveText();
        }

        if (transform.childCount < 2)
        {
            switch (collision.gameObject.tag)
            {
                case "pork":
                    OnParent(collision);
                    break;
                case "aji":
                    OnParent(collision);
                    break;
                case "onion":
                    OnParent(collision);
                    break;
                case "chicken":
                    OnParent(collision);
                    break;
                case "nasu":
                    OnParent(collision);
                    break;
                case "oba":
                    OnParent(collision);
                    break;
            }
        }
    }

    void OnCollisionStay(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "pork":
                OnFrag();
                break;
            case "aji":
                OnFrag();
                break;
            case "onion":
                OnFrag();
                break;
            case "chicken":
                OnFrag();
                break;
            case "nasu":
                OnFrag();
                break;
            case "oba":
                OnFrag();
                break;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "pork":
                OffParent(collision);
                break;
            case "aji":
                OffParent(collision);
                break;
            case "onion":
                OffParent(collision);
                break;
            case "chicken":
                OffParent(collision);
                break;
            case "nasu":
                OffParent(collision);
                break;
            case "oba":
                OffParent(collision);
                break;
        }
    }

    void OnParent(Collision collision)
    {
        dropFrag = true;
        collision.gameObject.transform.parent = gameObject.transform;
        collision.gameObject.transform.localPosition = new Vector3(0, -0.13f, 0.12f);
    }
    void OffParent(Collision collision)
    {
        dropFrag = false;
    }
    void OnFrag()
    {
        dropFrag = true;
    }
    public void OnDrag()
    {
        isRigidbody.useGravity = false;
        isRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        TapPos = Input.mousePosition;
        TapPos.y = Mathf.Clamp(TapPos.y, 200, 1000);
        TapPos.z = 1.15f - Mathf.Abs(transform.position.x) / 4;
        transform.position = Camera.main.ScreenToWorldPoint(TapPos) + new Vector3(0, 0, 0.1f);
        gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
        if (Camera.main.ScreenToWorldPoint(TapPos).x > 0)
        {
            Left_Arm.transform.position = Re_LeftArm;
            Right_Arm.transform.position = Camera.main.ScreenToWorldPoint(TapPos);
        }
        else
        {
            Right_Arm.transform.position = Re_RightArm;
            Left_Arm.transform.position = Camera.main.ScreenToWorldPoint(TapPos);
        }

    }

    public void OffDrag()
    {
        isRigidbody.useGravity = true;
        isRigidbody.constraints = RigidbodyConstraints.None;
        if (Camera.main.ScreenToWorldPoint(TapPos).x > 0)
        {
            Right_Arm.transform.position = Re_RightArm;
        }
        else
        {
            Left_Arm.transform.position = Re_LeftArm;
        }
    }
}
