﻿using UnityEngine;

public class Amipop : MonoBehaviour
{
    [SerializeField] GameObject amispawn;
    [SerializeField] GameObject ami;
    new Rigidbody rigidbody;

    void Start()
    {
        rigidbody = ami.GetComponent<Rigidbody>();
    }

    public void Pop()
    {
        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        ami.transform.DetachChildren();
        ami.transform.position = amispawn.transform.position;
        ami.transform.rotation = amispawn.transform.rotation;
    }
}
