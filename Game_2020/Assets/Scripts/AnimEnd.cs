﻿using UnityEngine;

public class AnimEnd : MonoBehaviour
{
    public int stage = 0;

    public void EEE()
    {
        if(PlayerPrefs.GetInt("Tyuto") == 0)
        {
            Invoke("TyutorialGo", 1f);
        }
        else
        {
            Invoke("NectScne", 1f);
        }
    }

    public void NectScne()
    {
        FadeManager.FadeOut(5);
    }

    public void TyutorialGo()
    {
        PlayerPrefs.SetInt("Tyuto", 1);
        FadeManager.FadeOut(7);
    }
}
