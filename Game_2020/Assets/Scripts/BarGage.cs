﻿using UnityEngine;
using UnityEngine.UI;

public class BarGage : MonoBehaviour
{

    [SerializeField] Slider Slider;
    public float sli_value;

    void Start()
    {
        sli_value = Slider.value;
    }

    public void SliderVarChange(int value)
    {
        Slider.value = Slider.value - value;
        sli_value = Slider.value;
    }
}
