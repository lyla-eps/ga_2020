﻿using UnityEngine;

public class COL : MonoBehaviour
{
    bool ch = true;

    private void OnCollisionEnter(Collision collision)
    {
        if (ch == true)
        {
            Clone(collision.gameObject);
            ch = false;
        }
    }

    public GameObject Clone(GameObject obj)
    {
        var clone = Instantiate(obj) as GameObject;
        clone.transform.parent = obj.transform.parent;
        clone.transform.localPosition = obj.transform.localPosition;
        clone.transform.localScale = obj.transform.localScale;
        return clone;
    }
}
