﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class Camera_Cont : MonoBehaviour
{
    [SerializeField] GameObject Left_Arm,Right_Arm,Po;

    [SerializeField] Animator anim;

    // カメラオブジェクトを格納する変数
    public Camera mainCamera;
    // カメラの回転速度を格納する変数
    public Vector2 rotationSpeed;
    // マウス座標を格納する変数
    private Vector2 lastMousePosition;
    // カメラの角度を格納する変数（初期値に0,0を代入）
    private Vector2 newAngle = new Vector2(0, 0);
    private Vector2 newArmAngle = new Vector2(0, 0);

    //pose中かどうかを判断する変数
    public bool Pose_Now = false;
    [SerializeField] GameObject Pose_Screen;
    public bool Touch = false;
    //鍋をクリックしたときに使用する変数
    public bool Click_Now = false;
    [SerializeField] PostEffect cameraConponent;
    [SerializeField] AudioMixerSnapshot normalSound;
    [SerializeField] AudioMixerSnapshot nabeSound;

    [SerializeField] OutlinePostEffect ou;

    [SerializeField] GameObject Yes_1, Yes_2, No_1, No_2, Lain_1, Lain_2;


    Vector3 angle;
    int I_Potion = 1;

    void Update()
    {
        //カメラの視点移動処理
        if (Pose_Now == false && Click_Now == false)
        {
            Camm();
        }

        //スペースキーで中断する処理
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Pose_Now == false)
            {
                Pose_Now = true;
                anim.SetBool("Back", true);
                Invoke("TimeTT", 5f);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Game_Close();
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            FadeManager.FadeOut(4);
        }
    }

    public void Game_Close()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		    Application.Quit();
#endif
    }

    public void TimeTT()
    {
        SetChenge(true);
    }

    public void Yes_taiki()
    {
        SetChenge(true);
    }

    public void No_taiki()
    {
        SetChenge(false);
    }
        
    public void SetChenge(bool b)
    {
        Yes_1.SetActive(b);
        Yes_2.SetActive(!b);
        No_1.SetActive(b);
        No_2.SetActive(!b);
        Lain_1.SetActive(b);
        Lain_2.SetActive(!b);
    }

    public void TimeTrue()
    {
        Pose_Now = false;
        anim.SetBool("Back", false);
    }

    //左の鍋に視点を合わせる
    public void LeftCamera()
    {
        if (Click_Now == false)
        {
            Click_Now = true;
            I_Potion = 1;
            StartCoroutine(UpdateRotation(35, -40, 1f));
            Invoke("ColutinStart", 1f);
            cameraConponent.enabled = true;
            ou.enabled = true;
        }
    }

    //真ん中の鍋に視点を合わせる
    public void ManakaCamera()
    {
        if (Click_Now == false)
        {
            Click_Now = true;
            I_Potion = 2;
            StartCoroutine(UpdateRotation(35, 0, 1f));
            Invoke("ColutinStart", 1f);
            cameraConponent.enabled = true;
            ou.enabled = true;
        }
    }

    //右の鍋に視点を合わせる
    public void RightCamera()
    {
        if (Click_Now == false)
        {
            Click_Now = true;
            cameraConponent.enabled = true;
            ou.enabled = true;
            I_Potion = 3;
            StartCoroutine(UpdateRotation(35, 60, 1f));
            Invoke("ColutinStart", 1f);
        }
    }

    //I_Potionがどこの鍋に向かっていくのかを示している。1 == 左　2 == 真ん中 3 == 右
    public void ColutinStart()
    {
        Left_Arm.SetActive(false);
        Right_Arm.SetActive(false);

        nabeSound.TransitionTo(1f);
        switch (I_Potion)
        {
            case 1:
                StartCoroutine(UpdateRotation(60, -40, 1f));
                StartCoroutine(UpdatePosition(this.transform.position, new Vector3(-0.545f, 1.5f, 0.5f), 1f));
                break;
            case 2:
                 StartCoroutine(UpdateRotation(60, 0, 1f));
                StartCoroutine(UpdatePosition(this.transform.position, new Vector3(0, 1.4f, 0.85f), 1f));
                break;
            case 3:
                StartCoroutine(UpdateRotation(60, 60, 1f));
                StartCoroutine(UpdatePosition(this.transform.position, new Vector3(0.455f, 1.5f, 0.6f), 1f));
                break;
        }
    }

    //通常支店に戻す
    public void ReturnCamera()
    {
        normalSound.TransitionTo(1f);
        if (Click_Now)//通常カメラに戻る処理
        {
            switch (I_Potion)
            {
                case 1:
                    StartCoroutine(UpdateRotation(35, -40, 1f));
                    
                    break;
                case 2:
                    StartCoroutine(UpdateRotation(35, 0, 1f));
                    break;
                case 3:
                    StartCoroutine(UpdateRotation(35, 60, 1f));
                    break;
            }
            StartCoroutine(UpdatePosition(this.transform.position, new Vector3(0, 1.7f, 0), 1f));

            Left_Arm.SetActive(true);
            Right_Arm.SetActive(true);

            cameraConponent.enabled = false;
            ou.enabled = false;
            Click_Now = false;
        }
    }

    public void Rotation(float potion_x, float potion_y, float i_time)
    {
        StartCoroutine(UpdateRotation(potion_x,potion_y, i_time));
    }

    //potion_Xが現在のカメラの位置の角度X、potion_yが現在のカメラの位置の角度Ｙ
    public IEnumerator UpdateRotation(float potion_x,float potion_y,float i_time)
    {
        angle = this.transform.eulerAngles;
        float startTime = Time.time;
        do
        {
            float timeStep = i_time > 0.0f ? (Time.time - startTime) / i_time : 1.0f;
            this.transform.localEulerAngles = new Vector2(Mathf.LerpAngle(angle.x, potion_x, timeStep), Mathf.LerpAngle(angle.y, potion_y, timeStep));
            yield return null;
        }
        while (Time.time < startTime + i_time);
        transform.localEulerAngles = new Vector2(potion_x, potion_y);
    }

    private IEnumerator UpdatePosition(Vector3 i_startPosition, Vector3 i_targetPosition, float i_time)
    {
        float startTime = Time.time;
        do
        {
            float timeStep = i_time > 0.0f ? (Time.time - startTime) / i_time : 1.0f;
            this.transform.position = Vector3.Lerp(i_startPosition, i_targetPosition, timeStep);
            yield return null;
        }
        while (Time.time < startTime + i_time);
    }

    //カメラの移動
    public void Camm()
    {
        // 左クリックした時
        if (Input.GetMouseButtonDown(0))
        {
            // カメラの角度を変数"newAngle"に格納
            newAngle = mainCamera.transform.localEulerAngles;
            newArmAngle = Left_Arm.transform.localEulerAngles;

            // マウス座標を変数"lastMousePosition"に格納
            lastMousePosition = Input.mousePosition;
        }
        // 左ドラッグしている間
        else if (Input.GetMouseButton(0))
        {
            // Y軸の回転：マウスドラッグ方向に視点回転
            // マウスの水平移動値に変数"rotationSpeed"を掛ける
            //（クリック時の座標とマウス座標の現在値の差分値）
            newAngle.y -= (lastMousePosition.x - Input.mousePosition.x) * rotationSpeed.y;
            newArmAngle.y -= (lastMousePosition.x - Input.mousePosition.x) * rotationSpeed.y;

            // X軸の回転：マウスドラッグ方向に視点回転
            // マウスの垂直移動値に変数"rotationSpeed"を掛ける
            //（クリック時の座標とマウス座標の現在値の差分値）
            newAngle.x -= (Input.mousePosition.y - lastMousePosition.y) * rotationSpeed.x;
            // "newAngle"の角度をカメラ角度に格納
            mainCamera.transform.localEulerAngles = newAngle;
            Left_Arm.transform.localEulerAngles = newArmAngle;
            Right_Arm.transform.localEulerAngles = newArmAngle;

            // マウス座標を変数"lastMousePosition"に格納
            lastMousePosition = Input.mousePosition;
        }
    }

    public void ReMenu()
    {
        //var destruction_target = GameObject.FindWithTag("destroy_target");
        Destroy(GameObject.FindWithTag("destroy_target"));
        FadeManager.FadeOut(1);
    }
}
