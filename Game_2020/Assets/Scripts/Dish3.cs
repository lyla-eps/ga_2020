﻿using UnityEngine;

public class Dish3 : MonoBehaviour
{
    public GameObject obj;
    [SerializeField] GameObject firstPos;
    [SerializeField] GameObject secondPos;
    [SerializeField] GameObject thirdPos;
    [SerializeField] GameObject sideObj;
    GameObject firstObj, secondObj, thirdObj;
    
    int i = 2;

    void Start()
    {
        obj.GetComponent<Rigidbody>().useGravity = false;
        firstObj = obj;
        i = 2;
        firstPos.SetActive(true);
        obj.transform.parent = gameObject.transform;
        obj.transform.position = firstPos.transform.position;
        obj.transform.rotation = firstPos.transform.rotation;
        obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    public void Dishs()
    {
        switch (i)
        {
            case 2:
                obj.GetComponent<Rigidbody>().useGravity = false;
                secondObj = obj;
                secondPos.SetActive(true);
                obj.transform.parent = gameObject.transform;
                obj.transform.position = secondPos.transform.position;
                obj.transform.rotation = secondPos.transform.rotation;
                obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                i = 3;
                obj.gameObject.SetActive(false);
                break;
            case 3:
                gameObject.GetComponent<BoxCollider>().enabled = false;
                obj.GetComponent<Rigidbody>().useGravity = false;
                thirdObj = obj;
                thirdPos.SetActive(true);
                obj.transform.parent = gameObject.transform;
                obj.transform.position = thirdPos.transform.position;
                obj.transform.rotation = thirdPos.transform.rotation;
                obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                sideObj.SetActive(true);
                gameObject.transform.root.gameObject.GetComponent<SetAc>().Set();
                GameSetActive.dishCount++;
                Invoke("Desh", 1f);
                gameObject.transform.root.gameObject.GetComponent<Guest>().ScoreSet(Mathf.Min(firstObj.GetComponent<FriedTiming>().score, secondObj.GetComponent<FriedTiming>().score, thirdObj.GetComponent<FriedTiming>().score));
                obj.gameObject.SetActive(false);
                break;
        }
    }

    void Desh()
    {
        if (GameObject.Find("SM_bg_ageami") != null)
        {
            GameObject.Find("SM_bg_ageami").gameObject.transform.DetachChildren();
        }
        Destroy(gameObject);
    }
}
