﻿using UnityEngine;

public class DishChange : MonoBehaviour
{
    GameObject orignalfried;
    [SerializeField] GameObject guest;
    [SerializeField] GameObject[] dishs;
    Dish3 dish3;

    //Seki_Noをここに変えることでそのオブジェクトがどこの席に座っているかを確認できるようにする
    public int Seki_No;

    void Start()
    {
        dish3 = gameObject.GetComponent<Dish3>();
    }

    void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "pork":
                OnTrigger(other.gameObject);
                break;
            case "aji":
                OnTrigger(other.gameObject);
                break;
            case "onion":
                OnTrigger(other.gameObject);
                break;
            case "chicken":
                OnTrigger(other.gameObject);
                break;
            case "nasu":
                OnTrigger(other.gameObject);
                break;
            case "oba":
                OnTrigger(other.gameObject);
                break;
        }
    }

    void OnTriggerExit(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "pork":
                OffTrigger();
                break;
            case "aji":
                OffTrigger();
                break;
            case "onion":
                OffTrigger();
                break;
            case "chicken":
                OffTrigger();
                break;
            case "nasu":
                OffTrigger();
                break;
            case "oba":
                OffTrigger();
                break;
        }
    }

    void OnTrigger(GameObject other)
    {
        gameObject.layer = 12;
        if (gameObject.tag == "BaseDish")
        {
            switch (other.tag)
            {
                case "pork":
                    Dish(0, other);
                    break;
                case "aji":
                    Dish(1, other);
                    break;
                case "onion":
                    Dishs(2, other);
                    break;
                case "chicken":
                    Dishs(3, other);
                    break;
                case "nasu":
                    Dishs(4, other);
                    break;
                case "oba":
                    Dishs(5, other);
                    break;
            }
        }
        else
        {
            if (dish3.obj.tag == other.tag)
            {
                other.transform.parent = gameObject.transform;
                dish3.obj = other;
                dish3.Dishs();
            }
        }
    }

    void OffTrigger()
    {
        gameObject.layer = 0;
    }

    void Dish(int index, GameObject friedObj)
    {
        if (GameObject.Find("GameManager") != null)
        {
            TyutorialManager tyutorialManager = GameObject.Find("GameManager").GetComponent<TyutorialManager>();
            tyutorialManager.DishNext();
        }
        Guest guests = guest.GetComponent<Guest>();
        Debug.Log(index);
        guests.NameSet(index);
        guests.kyaku_No = Seki_No;
        
        GameObject cloneObject = Instantiate(dishs[index]);
        cloneObject.transform.position = gameObject.transform.position;
        cloneObject.transform.rotation = gameObject.transform.rotation;
        cloneObject.transform.parent = guest.transform;
        orignalfried = friedObj;
        orignalfried.transform.parent = cloneObject.transform;
        Test.fried = orignalfried;
        gameObject.SetActive(false);
    }

    void Dishs(int index, GameObject friedObj)
    {

        Guest guests = guest.GetComponent<Guest>();
        guests.NameSet(index);
        GameObject cloneObject = Instantiate(dishs[index]);
        cloneObject.transform.position = gameObject.transform.position;
        cloneObject.transform.rotation = gameObject.transform.rotation;
        cloneObject.transform.parent = guest.transform;
        orignalfried = friedObj;
        orignalfried.transform.parent = cloneObject.transform;
        cloneObject.transform.GetChild(0).GetComponent<Dish3>().obj = orignalfried;
        gameObject.SetActive(false);
    }

}
