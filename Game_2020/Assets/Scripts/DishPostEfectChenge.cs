﻿using UnityEngine;

public class DishPostEfectChenge : MonoBehaviour
{
    public void DishColorChengeOn()
    {
        this.gameObject.layer = 12;
    }
    
    public void DishColorChengeOff()
    {
        this.gameObject.layer = 0;
    }
}
