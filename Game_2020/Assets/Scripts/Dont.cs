﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Dont : MonoBehaviour
{

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex == 6)
        {
            Destroy(this.gameObject);
        }
    }

    public void Destroy()
    {
        SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetActiveScene());
    }
}
