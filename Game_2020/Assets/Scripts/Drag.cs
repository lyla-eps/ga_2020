﻿using UnityEngine;

public class Drag : MonoBehaviour
{
    Rigidbody isGravity;
    Vector3 TapPos;

    void Start()
    {
        isGravity = gameObject.GetComponent<Rigidbody>();
    }

    public void OnDrag()
    {
        isGravity.useGravity = false;
        TapPos = Input.mousePosition;
        TapPos.y = Mathf.Clamp(TapPos.y, 170, 500);
        TapPos.z = 1.3f - Mathf.Abs(transform.position.x) / 2;
        Debug.Log(TapPos.y + "/" + Camera.main.ScreenToWorldPoint(TapPos));
        transform.position = Camera.main.ScreenToWorldPoint(TapPos);
    }

    public void OffDrag()
    {
        isGravity.useGravity = true;
    }
}
