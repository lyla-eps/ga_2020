﻿using System.Collections;
using UnityEngine;

public class DragTest : MonoBehaviour
{
    GameObject Left_Arm, Right_Arm;
    [SerializeField] AudioSource audioSource;
    [SerializeField] FriedTiming FriedT;
    IEnumerator friedRoutine;
    Rigidbody isGravity;
    Vector3 TapPos;
    Vector3 Re_LeftArm, Re_RightArm;

    bool drag,tyu = false,age = false;
    bool isDrag = true;
    bool Lefthand_Check = false, Righthand_Check = false, Next_Check = false;

    void Start()
    {
        if (GameObject.Find("GameManager") != null)
        {
            tyu = true;
        }

        if (GameObject.Find("sm_ch_player_l") != null)
        {
            Left_Arm = GameObject.Find("sm_ch_player_l");
            Lefthand_Check = true;
        }
        if(GameObject.Find("sm_ch_player_r") != null)
        {
            Right_Arm = GameObject.Find("sm_ch_player_r");
            Righthand_Check = true;
        }
      
        isGravity = gameObject.GetComponent<Rigidbody>();
        if(Lefthand_Check == true)
        {
            Re_LeftArm = Left_Arm.transform.position;
        }

        if (Righthand_Check == true)
        {
            Re_RightArm = Right_Arm.transform.position;
        }

        friedRoutine = FriedT.FriedTime();
    }

    void Update()
    {
        if (drag)
        {
            TapPos = Input.mousePosition;
            TapPos.y = Mathf.Clamp(TapPos.y, 500, 1000);
            TapPos.z = 1.4f - Mathf.Abs(transform.position.x) / 2;
            transform.position = Camera.main.ScreenToWorldPoint(TapPos);
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Desk")
        {
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag == "Oil")
        {
            isDrag = false;
            audioSource.Play();
            StartCoroutine(friedRoutine);
            if (tyu == true)
            {
                TyutorialManager tyutorialManager = GameObject.Find("GameManager").GetComponent<TyutorialManager>();
                tyutorialManager.DropText();
                Next_Check = true;
                tyu = false;
            }
        }

        if (collision.gameObject.tag == "Ami")
        {
            isGravity.isKinematic = true;
            isGravity.constraints = RigidbodyConstraints.FreezeAll;
            gameObject.GetComponent<FriedTiming>().OnDrag();
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Oil")
        {
            audioSource.Stop();
            StopCoroutine(friedRoutine);
        }

        if (collision.gameObject.tag == "Ami")
        {
            age = true;
            isGravity.isKinematic = false;
            isGravity.constraints = RigidbodyConstraints.None;
        }
    }

    public void OnDrag()
    {
        if (tyu == true)
        {
            if (GameObject.Find("GameManager") != null)
            {
                TyutorialManager tyutorialManager = GameObject.Find("GameManager").GetComponent<TyutorialManager>();
                tyutorialManager.Right_Check = true;
                tyutorialManager.DropTrueText();
            }
        }

        if (isDrag)
        {
            if (age == true)
            {
                if (GameObject.Find("GameManager") != null)
                {
                    TyutorialManager tyutorialManager = GameObject.Find("GameManager").GetComponent<TyutorialManager>();
                    tyutorialManager.DishMoveText();
                }
            }

            isGravity.useGravity = false;
            drag = true;
            TapPos = Input.mousePosition;
            TapPos.y = Mathf.Clamp(TapPos.y, 500, 1000);
            TapPos.z = 1.3f - Mathf.Abs(transform.position.x) / 2;
            transform.position = Camera.main.ScreenToWorldPoint(TapPos);
            if (Camera.main.ScreenToWorldPoint(TapPos).x > 0)
            {
                Left_Arm.transform.position = Re_LeftArm;
                Right_Arm.transform.position = Camera.main.ScreenToWorldPoint(TapPos);
            }
            else
            {
                Right_Arm.transform.position = Re_RightArm;
                Left_Arm.transform.position = Camera.main.ScreenToWorldPoint(TapPos);
            }
        }
    }

    public void OffDrag()
    {

        isGravity.useGravity = true;
        drag = false;
        if (Camera.main.ScreenToWorldPoint(TapPos).x > 0)
        {
            Right_Arm.transform.position = Re_RightArm;
        }
        else
        {
            Left_Arm.transform.position = Re_LeftArm;
        }
    }

    public void EntStart()
    {
        this.gameObject.layer = 8;
    }

    public void EntEnd()
    {
        this.gameObject.layer = 0;
    }
}
