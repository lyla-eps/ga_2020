﻿using System.Collections;
using UnityEngine;

public class Eat : MonoBehaviour
{
    [SerializeField] GameObject Text1, Text2,Oya;

    public void EatStart()
    {
        StartCoroutine(EatIn());
    }

    IEnumerator EatIn()
    {
        Oya.SetActive(true);
        Text1.SetActive(true);
        yield return new WaitForSeconds(1f);
        Text1.SetActive(false);
        Text2.SetActive(true);
        yield return new WaitForSeconds(1f);
        Text2.SetActive(false);
        Oya.SetActive(false);
    }
}
