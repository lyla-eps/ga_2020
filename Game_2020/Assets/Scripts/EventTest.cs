﻿using UnityEngine;

public class EventTest : MonoBehaviour
{
    [SerializeField] GameObject MainCamera;
    Camera_Cont camera_;

    public void Start()
    {
       camera_ = MainCamera.GetComponent<Camera_Cont>();
    }

    //左の鍋に視点を合わせる
    public void LeftCameraTouch()
    {
        camera_.LeftCamera();
    }

    //真ん中の鍋に視点を合わせる
    public void ManakaCameraTouch()
    {
        camera_.ManakaCamera();
    }

    //右の鍋に視点を合わせる
    public void RigtCameraTouch()
    {
        camera_.RightCamera();
    }
}
