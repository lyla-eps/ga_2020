﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriger : MonoBehaviour,IPointerDownHandler
{
    [SerializeField] int CameraPotion_Namber;
    [SerializeField] Camera_Cont cm;
    [SerializeField] LeftOut LeftOut;
    [SerializeField] MidolOut MidolOut;
    [SerializeField] RightOut RightOut;

    public void OnPointerDown(PointerEventData eventData)
    {
        switch (CameraPotion_Namber)
        {
            case 1:
                cm.LeftCamera();
                break;
            case 2:
                cm.ManakaCamera();
                break;
            case 3:
                cm.RightCamera();
                break;
            case 4:
                cm.ReturnCamera();
                break;
        }
    }

    public void LeftTrue()
    {
        LeftOut.enabled = true;
    }

    public void LeftFalse()
    {
        LeftOut.enabled = false;
    }

    public void MidolTrue()
    {
        MidolOut.enabled = true;
    }

    public void MIdolFalse()
    {
        MidolOut.enabled = false;
    }

    public void RightTrue()
    {
        RightOut.enabled = true;
    }

    public void RightFalse()
    {
        RightOut.enabled = false;
    }
}
