﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeManager : MonoBehaviour
{
    /// <summary>フェード用のCanvas</summary>
    private static Canvas m_fadeCanvas;
    /// <summary>フェード用のImage</summary>
    private static Image m_fadeImage;

    /// <summary>フェードインのフラグ</summary>
    public static bool b_isFadeIn = false;
    /// <summary>フェードアウトのフラグ</summary>
    public static bool b_isFadeOut = false;

    /// <summary>フェードしたい時間（単位は秒）</summary>
    private static float f_fadeTime = 1.5f;
    /// <summary>フェード用Imageの透明度</summary>
    private static float f_alpha = 0.0f;

    /// <summary>遷移先のシーン番号</summary>
    private static int i_nextScene;

    /// <summary>
    /// フェード用のCanvasとImage生成
    /// </summary>
    static void Init()
    {
        //フェード用のCanvasObject生成
        GameObject FadeCanvasObject = new GameObject("CanvasFade");
        m_fadeCanvas = FadeCanvasObject.AddComponent<Canvas>();
        FadeCanvasObject.AddComponent<GraphicRaycaster>();
        m_fadeCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        FadeCanvasObject.AddComponent<FadeManager>();

        //最前面になるよう適当なソートオーダー設定
        m_fadeCanvas.sortingOrder = 100;

        //フェード用のImageObject生成
        m_fadeImage = new GameObject("ImageFade").AddComponent<Image>();
        m_fadeImage.transform.SetParent(m_fadeCanvas.transform, false);
        m_fadeImage.rectTransform.anchoredPosition = Vector3.zero;

        //Imageサイズは適当に大きく設定してください
        m_fadeImage.rectTransform.sizeDelta = new Vector2(9999, 9999);
    }

    /// <summary>
    /// フェードイン開始
    /// </summary>
    public static void FadeIn()
    {
        if (m_fadeImage == null) Init();
        m_fadeImage.color = Color.black;
        b_isFadeIn = true;
    }

    /// <summary>
    /// フェードアウト開始
    /// </summary>
    /// <param name="n">遷移先のシーン番号</param>
    public static void FadeOut(int n)
    {
        if (m_fadeImage == null) Init();
        i_nextScene = n;
        m_fadeImage.color = Color.clear;
        m_fadeCanvas.enabled = true;
        b_isFadeOut = true;
    }

    void Update()
    {
        //フラグ有効なら毎フレームフェードイン/アウト処理
        if (b_isFadeIn)
        {
            //経過時間から透明度計算
            f_alpha -= Time.deltaTime / f_fadeTime;

            //フェードイン終了判定
            if (f_alpha <= 0.0f)
            {
                b_isFadeIn = false;
                f_alpha = 0.0f;
                m_fadeCanvas.enabled = false;
            }

            //フェード用Imageの色・透明度設定
            m_fadeImage.color = new Color(0.0f, 0.0f, 0.0f, f_alpha);
        }
        else if (b_isFadeOut)
        {
            //経過時間から透明度計算
            f_alpha += Time.deltaTime / f_fadeTime;

            //フェードアウト終了判定
            if (f_alpha >= 1.0f)
            {
                b_isFadeOut = false;
                f_alpha = 1.0f;

                //次のシーンへ遷移
                SceneManager.LoadScene(i_nextScene);
            }

            //フェード用Imageの色・透明度設定
            m_fadeImage.color = new Color(0.0f, 0.0f, 0.0f, f_alpha);
        }
    }
}