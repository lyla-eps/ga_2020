﻿using UnityEngine;

public class Fried : MonoBehaviour
{
    [SerializeField] Material MidolOil;
    [SerializeField] Material badOil;

    public int i = 1;
  
    public void OileChenge()
    {
        switch (i)
        {
            case 1:
                transform.GetChild(0).gameObject.GetComponent<Renderer>().sharedMaterial = MidolOil;
                i = 2;
                break;
            case 2:
                transform.GetChild(0).gameObject.GetComponent<Renderer>().sharedMaterial = badOil;
                break;
        }
    }
}