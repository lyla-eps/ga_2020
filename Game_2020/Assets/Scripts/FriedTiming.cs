﻿using System.Collections;
using UnityEngine;

public class FriedTiming : MonoBehaviour
{
    [SerializeField] Mesh original_fri;
    MeshFilter original_row;
    AudioSource audioSource;

    [SerializeField] float friedTime;
    [SerializeField] float randomTIme;
    public int score = 0;
    public int par = 0;
    bool onfried;

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        original_row = gameObject.GetComponent<MeshFilter>();
    }

    public void OnDrag()
    {
        if(onfried) original_row.mesh = original_fri;
    }

    public IEnumerator FriedTime()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        float time = friedTime + Random.Range(-randomTIme, randomTIme + 1);
        onfried = false;
        score = 1;
        par = 1;
        audioSource.pitch = 3;
        yield return new WaitForSeconds(time / 3);
        onfried = false;
        score = 2;
        par = 5;
        audioSource.pitch = 2.5f;
        yield return new WaitForSeconds(time / 2 - time / 3);
        onfried = false;
        score = 5;
        par = 10;
        audioSource.pitch = 2;
        yield return new WaitForSeconds(time - time / 2 - time / 3);
        onfried = true;
        score = 10;
        par = 35;
        audioSource.pitch = 1;
        yield return new WaitForSeconds(time / 3);
        score = 8;
        par = 20;
        audioSource.pitch = 2;
        yield return new WaitForSeconds(time - time / 3);
        score = 1;
        par = 1;
        audioSource.pitch = 3;
    }
}
