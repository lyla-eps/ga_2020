﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameClose : MonoBehaviour
{
    [SerializeField] GameObject Yes_High, Yes_Down, No_High, No_Down;

    public void Yes_Chenge()
    {
        Chenge(true, false, false, true);
    }

    public void No_Chenge()
    {
        Chenge(false, true, true, false);
    }

    public void Chenge(bool yes_h, bool yes_d, bool no_h, bool no_d)
    {
        Yes_High.SetActive(yes_h);
        Yes_Down.SetActive(yes_d);
        No_High.SetActive(no_h);
        No_Down.SetActive(no_d);
    }

    public void YesEnter()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
		    Application.Quit();
#endif
    }

    public void NoEnter()
    {
        Chenge(true, false, false, true);
        gameObject.SetActive(false);
    }
}
