﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSetActive : MonoBehaviour
{
    [SerializeField] GameObject easy_game, normal_game, hard_game;
    [SerializeField] GameObject easy_game_set, normal_game_set1, normal_game_set2, hard_game_set1, hard_game_set2, hard_game_set3;
    [SerializeField] GameObject finishText;
    static public int dishCount = 0;

    void Awake()
    {
        dishCount = 0;
        switch (PlayerPrefs.GetInt("Kyaku"))
        {
            case 1:
                easy_game.SetActive(true);
                easy_game_set.SetActive(true);
                break;
            case 2:
                normal_game.SetActive(true);
                normal_game_set1.SetActive(true);
                normal_game_set2.SetActive(true);
                break;
            case 3:
                hard_game.SetActive(true);
                hard_game_set1.SetActive(true);
                hard_game_set2.SetActive(true);
                hard_game_set3.SetActive(true);
                break;
        }
    }

    void Update()
    {
        switch (PlayerPrefs.GetInt("Kyaku"))
        {
            case 1:
                if (dishCount == 3)
                    StartCoroutine(Finish());
                break;
            case 2:
                if (dishCount == 6)
                {
                    StartCoroutine(Finish());
                }
                break;
            case 3:
                if (dishCount == 9)
                    StartCoroutine(Finish());
                break;
        }
    }

    IEnumerator Finish()
    {
        if(GameObject.Find("DontMusicObj") != null)
        {
            Dont dont = GameObject.Find("DontMusicObj").GetComponent<Dont>();
            dont.Destroy();
        }
        finishText.SetActive(true);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(6);
    }
}
