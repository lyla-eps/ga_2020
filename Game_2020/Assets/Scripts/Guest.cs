﻿using UnityEngine;

public class Guest : MonoBehaviour
{
    [SerializeField] BarGage BarGage;
    public int[] i_name = new int[3];
    public int[] i_score = new int[3];
    int n = 0,i = 0, chenge_No = 0,name_index = 0;
    public int kyaku_No = 0;
    public float slider_value = 100f;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Update()
    {
        if(chenge_No >= 2)
        {
            chenge_No = 0;
            NameChenge();
        }
        slider_value = BarGage.sli_value;
    }

    //名前をセットする
    public void NameSet(int name_nam)
    {
        i_name[n] = name_nam;
        n++;
        chenge_No++;
    }
    
    //スコアをセットする
    public void ScoreSet(int score)
    {
        i_score[i] = score;
        Debug.Log(i_score[i]);
        i++;
        chenge_No++;
    }

    //スコアをそれぞれの人数の時に％テージを変えてゲージに渡す
    //お客さんの人数はPlayerPrephasのKyakuが持ってる
    //お客さんが欲しいものはPlayerPrefs.SetInt("WantSeki1")がもってる
    public void NameChenge()
    {
        ///客の人数で処理を切り替え
        switch (kyaku_No)
        {
            case 1:
                if(PlayerPrefs.GetInt("WantSeki1") == i_name[name_index])
                {
                    ScoreTrueChenge();
                }
                else
                {
                    ScoreFalseChenge();
                }
                break;
            case 2:
                if (PlayerPrefs.GetInt("WantSeki2") == i_name[name_index])
                {
                    ScoreTrueChenge();
                }
                else
                {
                    ScoreFalseChenge();
                }
                break;
            case 3:
                if (PlayerPrefs.GetInt("WantSeki3") == i_name[name_index])
                {
                    ScoreTrueChenge();
                }
                else
                {
                    ScoreFalseChenge();
                }
                break;
        }
    }

    //提供したものが一緒がだった場合
    public void ScoreTrueChenge()
    {
        switch (PlayerPrefs.GetInt("Kyaku"))
        {
            case 1:
                switch (i_score[name_index])
                {
                    case 10:
                        BarGage.SliderVarChange(35);
                        break;
                    case 8:
                        BarGage.SliderVarChange(20);
                        break;
                    case 5:
                        BarGage.SliderVarChange(10);
                        break;
                    case 2:
                        BarGage.SliderVarChange(5);
                        break;
                    case 1:
                        BarGage.SliderVarChange(1);
                        break;
                }
                break;
            case 2:
                switch (i_score[name_index])
                {
                    case 10:
                        BarGage.SliderVarChange(18);
                        break;
                    case 8:
                        BarGage.SliderVarChange(10);
                        break;
                    case 5:
                        BarGage.SliderVarChange(5);
                        break;
                    case 2:
                        BarGage.SliderVarChange(3);
                        break;
                    case 1:
                        BarGage.SliderVarChange(1);
                        break;
                }
                break;
            case 3:
                switch (i_score[name_index])
                {
                    case 10:
                        BarGage.SliderVarChange(12);
                        break;
                    case 8:
                        BarGage.SliderVarChange(7);
                        break;
                    case 5:
                        BarGage.SliderVarChange(4);
                        break;
                    case 2:
                        BarGage.SliderVarChange(2);
                        break;
                    case 1:
                        BarGage.SliderVarChange(1);
                        break;
                }
                break;
        }
        name_index++;
    }

    //提供したものが一緒じゃない場合
    public void ScoreFalseChenge()
    {
        switch (PlayerPrefs.GetInt("Kyaku"))
        {
            case 1:
                switch (i_score[name_index])
                {
                    case 10:
                        BarGage.SliderVarChange(30);
                        break;
                    case 8:
                        BarGage.SliderVarChange(15);
                        break;
                    case 5:
                        BarGage.SliderVarChange(5);
                        break;
                    case 2:
                        BarGage.SliderVarChange(1);
                        break;
                    case 1:
                        BarGage.SliderVarChange(1);
                        break;
                }
                break;
            case 2:
                switch (i_score[name_index])
                {
                    case 10:
                        BarGage.SliderVarChange(15);
                        break;
                    case 8:
                        BarGage.SliderVarChange(8);
                        break;
                    case 5:
                        BarGage.SliderVarChange(3);
                        break;
                    case 2:
                        BarGage.SliderVarChange(1);
                        break;
                    case 1:
                        BarGage.SliderVarChange(1);
                        break;
                }
                break;
            case 3:
                switch (i_score[name_index])
                {
                    case 10:
                        BarGage.SliderVarChange(10);
                        break;
                    case 8:
                        BarGage.SliderVarChange(3);
                        break;
                    case 5:
                        BarGage.SliderVarChange(2);
                        break;
                    case 2:
                        BarGage.SliderVarChange(1);
                        break;
                    case 1:
                        BarGage.SliderVarChange(1);
                        break;
                }
                break;
        }
        name_index++;
    }

}
