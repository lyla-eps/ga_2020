﻿using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField] GameObject Cus, Nos;
    [SerializeField] Animator anim;
    [SerializeField] GameObject text, text1,text2;
    bool ttt = true;
    bool textSet = false;

    [SerializeField] GameObject Hensei_Normal, Hensei_Highlight;
    bool hilightcheck = false;
    
    public void Open()
    {
        if(ttt == true)
        {
            hilightcheck = true;
        }
        else
        {
            hilightcheck = false;
        }
        

        text2.SetActive(false);
        Cus.SetActive(ttt);
        ttt = !ttt;
        text.SetActive(false);
        text1.SetActive(true);
        
        if (!textSet)
        {
            anim.Play("serifu");
        }
        textSet = true;
    }

    public void Close()
    {
        Nos.SetActive(false);
        Open();
    }

    public void HighlihgtOn()
    {
        Hensei_Normal.SetActive(false);
        Hensei_Highlight.SetActive(true);
    }

    public void HightlightOff()
    {
        if(hilightcheck == false)
        {
            Hensei_Highlight.SetActive(false);
            Hensei_Normal.SetActive(true);
        }
    }
}
