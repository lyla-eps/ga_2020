﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Menu_Drop : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    //ドロップするオブジェクト操作のスクリプト
    //オブジェクトは全部で６
    //ドロップオブジェクトを指定位置に置くことでその場所のオブジェクトをTrueにする

    public Transform defaultParent;
    int i;

    public void OnBeginDrag(PointerEventData eventData)
    {
        defaultParent = this.transform.parent;
        i = transform.GetSiblingIndex();
        transform.SetParent(defaultParent.parent, false);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(defaultParent, false);
        transform.SetSiblingIndex(i);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}
