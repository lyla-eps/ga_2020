﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu_Manager : MonoBehaviour
{

    //　非同期動作で使用するAsyncOperation
    private AsyncOperation async;
    //　シーンロード中に表示するUI画面
    [SerializeField]
    private GameObject loadUI;
    //　読み込み率を表示するスライダー
    [SerializeField]
    private Slider slider;

    //string menu1 = "アジフライ", menu2 = "とんかつ", menu3 = "唐揚げ", menu4 = "なす", menu5 = "たまねぎ", menu6 = "大葉";
    string menu1 = "とんかつ", menu2 = "アジフライ", menu3 = "たまねぎ", menu4 = "唐揚げ", menu5 = "なす", menu6 = "大葉";
    int m_1, m_2_1, m_2_2, m_3_1, m_3_2, m_3_3;
    [SerializeField] Text Menu_Text1, Menu_Text2, Menu_Text3,Kettei_Text;
    [SerializeField] GameObject Kettei_Obj, No_Obj, Syou_Syou, Syou_Dai, Tiku_Syou, Tiku_Dai, Bay_Syou, Bay_Dai;
    int Ichiban_Syoku_No = 0, Niban_Syoku_No = 0, Sanban_Syoku_No = 0,Kyaku_Ninzu = 0;
    public int Custm_Ichi_Syoku_No = 0, Custm_Niban_Syoku_No = 0, Custm_San_Syoku_No = 0;
    [SerializeField] AudioSource bgm,se_kettei,se_re;
    int volume = 0;

    //------------------------------------------------------------------------------------------------
    [SerializeField] GameObject menu_induction_text, menu_induciton_text1,menu_induction_text2;  //メニューの操作誘導用テキスト
    bool guidance_decision = false;
    //------------------------------------------------------------------------------------------------


    private void Awake()
    {
        PlayerPrefs.SetInt("Gage", 0);
        //1人用ステージ決定
        m_1 = Random.Range(0, 2);
        //2人用ステージ決定
         m_2_1 = Random.Range(0, 2);
         m_2_2 = Random.Range(0, 2);
        //3人用ステージ決定
         m_3_1 = Random.Range(0, 3);
         m_3_2 = Random.Range(0, 3);
         m_3_3 = Random.Range(0, 3);
        One_Game();
    }

    private void Start()
    {
        
    }

    public void One_Game()
    {
        Rest(true,false,false);
        if (m_1 == 0)
        {
            Menu_Text1.text = $"壱番席ー{menu1}";
            Ichiban_Syoku_No = 1;
        }
        else
        {
            Menu_Text1.text = $"壱番席ー{menu2}";
            Ichiban_Syoku_No = 2;
        }
        Niban_Syoku_No = 0;
        Sanban_Syoku_No = 0;
    }

    public void Two_Game()
    {
        Rest(false,true,false);
        if (m_2_1 == 0)
        {
            Menu_Text2.text = $"弐番席ー{menu1}";
            Niban_Syoku_No = 1;
        }
        else
        {
            Menu_Text2.text = $"弐番席ー{menu2}";
            Niban_Syoku_No = 2;
        }

        if (m_2_2 == 0)
        {
            Menu_Text3.text = $"参番席ー{menu4}";
            Sanban_Syoku_No = 4;
        }
        else
        {
            Menu_Text3.text = $"参番席ー{menu5}";
            Sanban_Syoku_No = 5;
        }
        Ichiban_Syoku_No = 0;
    }

    public void Three()
    {
        Rest(false, false ,true);
        if (m_3_1 == 0)
        {
            Menu_Text1.text = $"壱番席ー{menu2}";
            Ichiban_Syoku_No =2;
        }
        else if (m_3_1 == 1)
        {
            Menu_Text1.text = $"壱番席ー{menu3}";
            Ichiban_Syoku_No = 3;
        }
        else
        {
            Menu_Text1.text = $"壱番席ー{menu6}";
            Ichiban_Syoku_No = 6;
        }

        if (m_3_2 == 0)
        {
            Menu_Text2.text = $"弐番席ー{menu4}";
            Niban_Syoku_No = 4;
        }
        else if (m_3_2 == 1)
        {
            Menu_Text2.text = $"弐番席ー{menu5}";
            Niban_Syoku_No = 5;
        }
        else
        {
            Menu_Text2.text = $"弐番席ー{menu1}";
            Niban_Syoku_No = 1;
        }

        if (m_3_3 == 0)
        {
            Menu_Text3.text = $"参番席ー{menu6}";
            Sanban_Syoku_No = 6;
        }
        else if (m_3_3 == 1)
        {
            Menu_Text3.text = $"参番席ー{menu4}";
            Sanban_Syoku_No = 4;
        }
        else
        {
            Menu_Text3.text = $"参番席ー{menu5}";
            Sanban_Syoku_No = 5;
        }

    }

    public void Rest(bool Syou,bool Tiku, bool Bai)
    {
        Menu_Text1.text = $"";
        Menu_Text2.text = $"";
        Menu_Text3.text = $"";
        Syou_Syou.SetActive(!Syou);
        Syou_Dai.SetActive(Syou);
        Tiku_Syou.SetActive(!Tiku);
        Tiku_Dai.SetActive(Tiku);
        Bay_Syou.SetActive(!Bai);
        Bay_Dai.SetActive(Bai);
    }

    public void One_Chenge()
    {
        Kettei_Text.text = "揚げ物好きおじいさん(来店人数1人)";
        Kyaku_Ninzu = 1;
        K_True();
    }

    public void Two_Chenge()
    {
        Kettei_Text.text = "老夫婦(来店人数2人)";
        Kyaku_Ninzu = 2;

        K_True();
    }

    public void Three_Chenge()
    {
        Kettei_Text.text = "お金持ち家族(来店人数3人)";
        Kyaku_Ninzu = 3;

        K_True();
    }

    public void K_True()
    {
        if (Custm_Ichi_Syoku_No == 0 || Custm_Niban_Syoku_No == 0 || Custm_San_Syoku_No == 0)
        {
            No_Obj.SetActive(true);

            //-----------------------------------------
            menu_induction_text2.SetActive(false);
            menu_induction_text.SetActive(false);
            if (!guidance_decision)
            {
                menu_induciton_text1.SetActive(true);
                guidance_decision = true;
            }
            //-----------------------------------------
        }
        else
        {
            Kettei_Obj.SetActive(true);

            
        }
    }

    public void K_False()
    {
        se_re.Play();
        Kettei_Obj.SetActive(false);
    }

    public void Game_Go()
    {
        PlayerPrefs.SetInt("WantSeki1", Ichiban_Syoku_No);
        PlayerPrefs.SetInt("WantSeki2", Niban_Syoku_No);
        PlayerPrefs.SetInt("WantSeki3", Sanban_Syoku_No);
        PlayerPrefs.SetInt("Syoku1", Custm_Ichi_Syoku_No);
        PlayerPrefs.SetInt("Syoku2", Custm_Niban_Syoku_No);
        PlayerPrefs.SetInt("Syoku3", Custm_San_Syoku_No);
        PlayerPrefs.SetInt("Kyaku", Kyaku_Ninzu);

        loadUI.SetActive(true);
        if(PlayerPrefs.GetInt("Tyuto") == 0)
        {
            StartCoroutine("Tyutolial_Next");
        }
        else
        {
            //　コルーチンを開始
            StartCoroutine("Game_Next");
        }
        
    }

    //フェードアウトさせてそのとき音もだんだん小さくさせる
    IEnumerator Game_Next()
    {
        if (GameObject.FindGameObjectWithTag("IchiSeki") != null)
        {
            SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("IchiSeki").gameObject, SceneManager.GetActiveScene());
            Destroy(GameObject.FindGameObjectWithTag("IchiSeki").gameObject);
        }

        if (GameObject.FindGameObjectWithTag("NiSeki") != null)
        {
            SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("NiSeki").gameObject, SceneManager.GetActiveScene());
            Destroy(GameObject.FindGameObjectWithTag("NiSeki").gameObject);
        }

        if (GameObject.FindGameObjectWithTag("SanSeki") != null)
        {
            SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("SanSeki").gameObject, SceneManager.GetActiveScene());
            Destroy(GameObject.FindGameObjectWithTag("SanSeki").gameObject);
        }

        yield return new WaitForSeconds(1f);
        if (Kyaku_Ninzu == 3)
        {
            async = SceneManager.LoadSceneAsync("EnterCut_Hard");
        }
        if (Kyaku_Ninzu == 2)
        {
            async = SceneManager.LoadSceneAsync("EnterCut_Normal");
        }
        if (Kyaku_Ninzu == 1)
        {
            async = SceneManager.LoadSceneAsync("EnterCut_Easy");
        }
        se_kettei.Play();

        while (!async.isDone)
        {
            var progressVal = Mathf.Clamp01(async.progress / 0.9f);
            slider.value = progressVal;
            yield return null;
        }

        for (; volume > 0; volume--)
        {
            bgm.volume = (float)(volume) / 100;
            yield return null;
        }
        bgm.volume = 0;
    }

    IEnumerator Tyutolial_Next()
    {
        yield return new WaitForSeconds(1f);

        async = SceneManager.LoadSceneAsync("EnterCut_Easy");

        se_kettei.Play();

        while (!async.isDone)
        {
            var progressVal = Mathf.Clamp01(async.progress / 0.9f);
            slider.value = progressVal;
            yield return null;
        }

        for (; volume > 0; volume--)
        {
            bgm.volume = (float)(volume) / 100;
            yield return null;
        }
        bgm.volume = 0;
    }
}