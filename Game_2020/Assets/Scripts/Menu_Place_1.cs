﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Menu_Place_1 : MonoBehaviour,IDropHandler
{
    [SerializeField] GameObject Azi, Ton, Kara, Tma, Oba, Nas;
    [SerializeField] Menu_Manager menu;
    TyutorialMenu tyu;

    //-----------------------------------------------------
    [SerializeField] GameObject Seat_Nom;
    //-----------------------------------------------------

    public void Start()
    {
        if (GameObject.FindGameObjectWithTag("Menu"))
        {
            tyu = GameObject.FindGameObjectWithTag("Menu").GetComponent<TyutorialMenu>();
        }
        else
        {

        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        switch (eventData.pointerDrag.transform.tag)
        {
            case "pork":
                ChengeActive1(true, false, false, false, false, false);
                menu.Custm_Ichi_Syoku_No = 1;
                //-----------------------------------------------------
                Seat_Nom.SetActive(false);
                //-----------------------------------------------------
                break;
            case "aji":
                ChengeActive1(false, true, false, false, false, false);
                menu.Custm_Ichi_Syoku_No = 2;
                //-----------------------------------------------------
                Seat_Nom.SetActive(false);
                //-----------------------------------------------------
                break;
            case "onion":
                ChengeActive1(false, false, true, false, false, false);
                menu.Custm_Ichi_Syoku_No = 3;
                //-----------------------------------------------------
                Seat_Nom.SetActive(false);
                //-----------------------------------------------------
                break;
            case "chicken":
                ChengeActive1(false, false, false, true, false, false);
                menu.Custm_Ichi_Syoku_No = 4;
                //-----------------------------------------------------
                Seat_Nom.SetActive(false);
                //-----------------------------------------------------
                break;
            case "nasu":
                ChengeActive1(false, false, false, false, true, false);
                menu.Custm_Ichi_Syoku_No = 5;
                //-----------------------------------------------------
                Seat_Nom.SetActive(false);
                //-----------------------------------------------------
                break;
            case "oba":
                ChengeActive1(false, false, false, false, false, true);
                menu.Custm_Ichi_Syoku_No = 6;
                //-----------------------------------------------------
                Seat_Nom.SetActive(false);
                //-----------------------------------------------------
                break;
        }
    }

    //pork aji onion chicken nasu oba
    public void ChengeActive1(bool pork, bool aji, bool onion, bool chiicken, bool nasu, bool oba)
    {
        if (GameObject.FindGameObjectWithTag("Menu"))
        {
            tyu = GameObject.FindGameObjectWithTag("Menu").GetComponent<TyutorialMenu>();
            if (tyu.firstcheck == false)
            {
                tyu.checkc += 1;
                tyu.firstcheck = true;
                tyu.SetOff();
            }
        }
        else
        {

        }
        Azi.SetActive(aji);
        Ton.SetActive(pork);
        Kara.SetActive(chiicken);
        Tma.SetActive(onion);
        Oba.SetActive(oba);
        Nas.SetActive(nasu);
    }
}
