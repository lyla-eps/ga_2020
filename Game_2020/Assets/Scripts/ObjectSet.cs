﻿using UnityEngine;

public class ObjectSet : MonoBehaviour
{
    [SerializeField] GameObject Syoku1, Syoku2, Syoku3, Syoku4, Syoku5, Syoku6;
    Transform Transform;
    int popCount = 0;
    int count = 0;

    void Start()
    {
        Transform = this.transform;
        ObjectSet1();
    }

    void OnCollisionEnter(Collision collision)
    {
        popCount++;
    }

    void OnCollisionExit(Collision collision)
    {
        count++;
        if (count == popCount) ObjectSet1();
    }

    void ObjectSet1()
    {
        switch (PlayerPrefs.GetInt("Syoku1"))
        {
            case 1:
                Instantiate(Syoku1, Transform);
                break;
            case 2:
                Instantiate(Syoku2, Transform);
                break;
            case 3:
                Instantiate(Syoku3, Transform);
                break;
            case 4:
                Instantiate(Syoku4, Transform);
                break;
            case 5:
                Instantiate(Syoku5, Transform);
                break;
            case 6:
                Instantiate(Syoku6, Transform);
                break;
        }
    }
}
