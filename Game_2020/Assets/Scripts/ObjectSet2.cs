﻿using UnityEngine;

public class ObjectSet2 : MonoBehaviour
{
    [SerializeField] GameObject Syoku1, Syoku2, Syoku3, Syoku4, Syoku5, Syoku6;
    Transform Transform;
    int popCount = 0;
    int count = 0;

    void Start()
    {
        Transform = this.transform;
        ObjectSet();
    }

    void OnCollisionEnter(Collision collision)
    {
        popCount++;
    }

    void OnCollisionExit(Collision collision)
    {
        count++;
        if (count == popCount) ObjectSet();
    }

    void ObjectSet()
    {
        switch (PlayerPrefs.GetInt("Syoku2"))
        {
            case 1:
                Instantiate(Syoku1,Transform);
                break;
            case 2:
                Instantiate(Syoku2, Transform);
                break;
            case 3:
                Instantiate(Syoku3, Transform);
                break;
            case 4:
                Instantiate(Syoku4, Transform);
                break;
            case 5:
                Instantiate(Syoku5, Transform);
                break;
            case 6:
                Instantiate(Syoku6, Transform);
                break;
        }
    }
}
