﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Oil_Controll : MonoBehaviour
{
    [SerializeField] GameObject leftPot;
    [SerializeField] GameObject centerPot;
    [SerializeField] GameObject rightPot;
    [SerializeField] Material GreatOil;
    [SerializeField] Button left;
    [SerializeField] Button center;
    [SerializeField] Button right;
    [SerializeField] Fried left_fried, midol_fired, right_fried;

    void Awake()
    {
        if (leftPot == null) Debug.LogError("LeftPot is empty.", leftPot);
        if (centerPot == null) Debug.LogError("CenterPot is empty.", centerPot);
        if (rightPot == null) Debug.LogError("RightPot is empty.", rightPot);
        if (GreatOil == null) Debug.LogError("GreatOil is empty.");
    }

    IEnumerator OilChenge(GameObject pot, Button button)
    {
        button.interactable = false;
        Debug.Log("GreatOil");
        yield return new WaitForSeconds(5);
        pot.GetComponent<Renderer>().sharedMaterial = GreatOil;
        button.interactable = true;
    }

    public void LeftOil()
    {
        left_fried.i = 1;
        StartCoroutine(OilChenge(leftPot, left));
    }

    public void CenterOil()
    {
        midol_fired.i = 1;
        StartCoroutine(OilChenge(centerPot, center));
    }

    public void RightOil()
    {
        right_fried.i = 1;
        StartCoroutine(OilChenge(rightPot, right));
    }
}
