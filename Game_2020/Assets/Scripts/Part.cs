﻿using System.Collections;
using UnityEngine;

public class Part : MonoBehaviour
{
    [SerializeField] GameObject ParticleSystem;

    void Start()
    {
        StartCoroutine("Res");
    }

    IEnumerator  Res()
    {
        yield return new WaitForSeconds(3f);
        ParticleSystem.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
}
