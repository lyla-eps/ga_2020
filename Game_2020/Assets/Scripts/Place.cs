﻿using UnityEngine;

public class Place : MonoBehaviour
{
    [SerializeField] GameObject BT1, BT2, BT3;
    [SerializeField] GameObject Custm_ImageIchi, Custm_ImageNi,Custm_ImageSan, Cus;
    bool cs1 = true, cs2 = true, cs3 = true;
    bool oon = false, bt1set = true,bt2set = true,bt3set = true;

    public void SetOn()
    {
        oon = !oon;
        Cus.SetActive(oon);
    }

    public void Open_Custms1()
    {
        if (oon == true)
        {
            BT1.SetActive(bt1set);
            BT2.SetActive(false);
            BT3.SetActive(false);
            Custm_ImageIchi.SetActive(cs1);
            Custm_ImageNi.SetActive(false);
            Custm_ImageSan.SetActive(false);
            cs1 = !cs1;
            if (bt2set == false)
            {
                bt2set = true;
            }

            if (bt3set == false)
            {
                bt3set = true;
            }

            bt1set = !bt1set;
        }
    }

    public void Open_Custms2()
    {
        if (oon == true)
        {
            BT1.SetActive(false);
            BT2.SetActive(bt2set);
            BT3.SetActive(false);
            Custm_ImageIchi.SetActive(false);
            Custm_ImageNi.SetActive(cs2);
            Custm_ImageSan.SetActive(false);
            cs2 = !cs2;
            if (bt1set == false)
            {
                bt1set = true;
            }

            if (bt3set == false)
            {
                bt3set = true;
            }
            bt2set = !bt2set;
        }
    }

    public void Open_Custms3()
    {
        if (oon == true)
        {
            BT1.SetActive(false);
            BT2.SetActive(false);
            BT3.SetActive(bt3set);
            Custm_ImageIchi.SetActive(false);
            Custm_ImageNi.SetActive(false);
            Custm_ImageSan.SetActive(cs3);
            cs3 = !cs3;
            if (bt2set == false)
            {
                bt2set = true;
            }

            if (bt1set == false)
            {
                bt1set = true;
            }
            bt3set = !bt3set;
        }
    }
}
