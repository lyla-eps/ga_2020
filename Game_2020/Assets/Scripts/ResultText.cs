﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System; //DateTimeを使用するため追加

public class ResultText : MonoBehaviour
{
    [SerializeField] Text Result_name;

    [SerializeField] Text OneonOne_name, OneonOn_Hoshi, OneonTwo_name, OneonTwo_Hoshi, OneonThree_name, OneonThree_Hoshi;

    [SerializeField] Text TwoonOne_name, TwoonOne_Hoshi,TwoonTwo_name,TwoonTwo_Hoshi, TwoonThree_name, TwoonThree_Hoshi;

    [SerializeField] Text ThreeonOne_name, ThreeonOne_Hoshi, ThreeonTwo_name,ThreeonTwo_Hoshi, ThreeonThree_name, ThreeonThree_Hoshi;

    [SerializeField] Text LastScore;

    [SerializeField] Text BounasText;


    string[] syoku_name = { "とんかつ","アジフライ","たまねぎ","からあげ","ナス","大葉"};

    private Guest guest_ichi, guest_ni, guest_san;

    private bool saigo = true, first = false, second = false, third = false;

    private int bounas_gage = 0;

    public int HoshiSan_Score = 0, HoshiNi_Score = 0, HoshiIchi_Score = 0;

    //--------------------------------------------------------------------------------------
    [SerializeField] Text _Calendar_Year, _Calendar_Month, _Calendar_Day;  //発効日用オブジェクト
    private int _Year, _Month, _Day;                                       //現在日時取得用
    //--------------------------------------------------------------------------------------

    private void Awake()
    {
        //--------------------------------------------------------------------------------------
        Calendar_Text_Set();    //日付
        //--------------------------------------------------------------------------------------

        switch (PlayerPrefs.GetInt("Kyaku"))
        {
            case 1:
                Result_name.text = "揚げ物好きおじいさん";
                break;
            case 2:
                Result_name.text = "老夫婦";
                break;
            case 3:
                Result_name.text = "お金持ち家族";
                break;
        }

        if (GameObject.FindGameObjectWithTag("IchiSeki") != null)
        {
            first = true;
            guest_ichi = GameObject.FindGameObjectWithTag("IchiSeki").GetComponent<Guest>();
            //SceneManager.MoveGameObjectToScene(guest_ichi.gameObject, SceneManager.GetActiveScene());
            OneonOne_name.text = syoku_name[guest_ichi.i_name[0]];
            OneonTwo_name.text = syoku_name[guest_ichi.i_name[1]];
            OneonThree_name.text = syoku_name[guest_ichi.i_name[2]];

            StarChenge(OneonOn_Hoshi, guest_ichi.i_score[0]);
            StarChenge(OneonTwo_Hoshi, guest_ichi.i_score[1]);
            StarChenge(OneonThree_Hoshi, guest_ichi.i_score[2]);

            if(saigo == true)
            {
                SougouHyoukaOn(1);
                saigo = false;
            }
        }

        
        if (GameObject.FindGameObjectWithTag("NiSeki") != null)
        {
            second = true;
            guest_ni = GameObject.FindGameObjectWithTag("NiSeki").GetComponent<Guest>();

            TwoonOne_name.text = syoku_name[guest_ni.i_name[0]];
            TwoonTwo_name.text = syoku_name[guest_ni.i_name[1]];
            TwoonThree_name.text = syoku_name[guest_ni.i_name[2]];

            StarChenge(TwoonOne_Hoshi, guest_ni.i_score[0]);
            StarChenge(TwoonTwo_Hoshi, guest_ni.i_score[1]);
            StarChenge(TwoonThree_Hoshi, guest_ni.i_score[2]);
            if (saigo == true)
            {
                SougouHyoukaOn(2);
                saigo = false;
            }
        }

        
        if (GameObject.FindGameObjectWithTag("SanSeki") != null)
        {
            third = true;
            guest_san = GameObject.FindGameObjectWithTag("SanSeki").GetComponent<Guest>();

            ThreeonOne_name.text = syoku_name[guest_san.i_name[0]];
            ThreeonTwo_name.text = syoku_name[guest_san.i_name[1]];
            ThreeonThree_name.text = syoku_name[guest_san.i_name[2]];

            StarChenge(ThreeonOne_Hoshi, guest_san.i_score[0]);
            StarChenge(ThreeonTwo_Hoshi, guest_san.i_score[1]);
            StarChenge(ThreeonThree_Hoshi, guest_san.i_score[2]);
            if (saigo == true)
            {
                SougouHyoukaOn(3);
                saigo = false;
            }
        }
    }

    public void StarChenge(Text text,int score)
    {
        switch (score)
        {
            case 10:
                text.text = "★★★★★";
                break;
            case 8:
                text.text = "★★★★　";
                break;
            case 5:
                text.text = "★★★　　";
                break;
            case 2:
                text.text = "★★　　　";
                break;
            case 1:
            case 0:
                text.text = "★　　　　";
                break;
        }
    }

    public void Bounas()
    {
        BounasText.text = "ボーナス不成立";
        if (PlayerPrefs.GetInt("Syoku1") == PlayerPrefs.GetInt("Syoku2") && PlayerPrefs.GetInt("Syoku2") == PlayerPrefs.GetInt("Syoku3"))
        {
            switch (PlayerPrefs.GetInt("Kyaku"))
            {
                case 1:
                    if (first == true)
                    {
                        if (guest_ichi.i_name[0] == guest_ichi.i_name[1] && guest_ichi.i_name[1] == guest_ichi.i_name[2])
                        {
                            if(guest_ichi.slider_value <= 50)
                            {
                                BounasText.text = "ボーナス成立";
                                bounas_gage = 20;
                            }
                        }
                    }
                    else
                    {
                        BounasText.text = "ボーナス不成立";
                    }
                    break;
                case 2:
                    if (second == true && third == true)
                    {
                        if (guest_san.i_name[0] == guest_san.i_name[1] && guest_san.i_name[1] == guest_san.i_name[2])
                        {
                            if (guest_ni.i_name[0] == guest_ni.i_name[1] && guest_ni.i_name[1] == guest_ni.i_name[2])
                            {
                                if(guest_ni.slider_value <= 50)
                                {
                                    BounasText.text = "ボーナス成立";
                                    bounas_gage = 20;
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    if (first == true && second == true && third == true)
                    {
                        if (guest_ichi.i_name[0] == guest_ichi.i_name[1] && guest_ichi.i_name[1] == guest_ichi.i_name[2])
                        {
                            if (guest_ni.i_name[0] == guest_ni.i_name[1] && guest_ni.i_name[1] == guest_ni.i_name[2])
                            {
                                if (guest_san.i_name[0] == guest_san.i_name[1] && guest_san.i_name[1] == guest_san.i_name[2])
                                {
                                    if(guest_san.slider_value <= 50)
                                    {
                                        BounasText.text = "ボーナス成立";
                                        bounas_gage = 20;
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    public void SougouHyoukaOn(int i)
    {
        switch (i)
        {
            case 1:
                SougouHyoukaOut(guest_ichi.slider_value);
                break;
            case 2:
                SougouHyoukaOut(guest_ni.slider_value);
                break;
            case 3:
                SougouHyoukaOut(guest_san.slider_value);
                break;
        }
    }

    public void SougouHyoukaOut(float value)
    {
        Bounas();
        value -= bounas_gage;

        HoshiIchi_Score = 100 - HoshiIchi_Score;
        HoshiNi_Score = 100 - HoshiNi_Score;
        HoshiSan_Score = 100 - HoshiSan_Score;

        Debug.Log(value);
        
        if (value <= HoshiSan_Score)//valueの値がHoshiSan_Score（80％以上）なら星３
        {
            LastScore.text = "★ ★ ★";
        }else if(value <= HoshiNi_Score)//valueの値がHoshiNi_Score（40％以上）なら星2
        {
            LastScore.text = "★ ★ 　";
        }else if(value <= HoshiIchi_Score)//valueの値がHoshiIchi_Score（0％以上）なら星1
        {
            LastScore.text = "★ 　 　";
        }
    }

    public void DestroyGameObject()
    {
        if(guest_ichi != null)
        {
            Destroy(guest_ichi.gameObject);
            SceneManager.MoveGameObjectToScene(guest_ichi.gameObject, SceneManager.GetActiveScene());
        }
        
        if(guest_ni != null)
        {
            Destroy(guest_ni.gameObject);
            SceneManager.MoveGameObjectToScene(guest_ni.gameObject, SceneManager.GetActiveScene());
        }

        if (guest_san != null)
        {
            Destroy(guest_san.gameObject);
            SceneManager.MoveGameObjectToScene(guest_san.gameObject, SceneManager.GetActiveScene());
        }
    }

    //--------------------------------------------------------------------------------------
    public void Calendar_Text_Set()
    {
        _Year = System.DateTime.Now.Year;
        _Month = System.DateTime.Now.Month;
        _Day = System.DateTime.Now.Day;
        _Calendar_Year.text = $"{_Year}";
        _Calendar_Month.text = $"{_Month}";
        _Calendar_Day.text = $"{_Day}";
    }
    //--------------------------------------------------------------------------------------
}
