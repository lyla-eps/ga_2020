﻿using UnityEngine;

public class Saibashi : MonoBehaviour
{
    Rigidbody isGravity;
    Vector3 TapPos;
    [SerializeField] GameObject Left_Arm, Right_Arm;
    Vector3 Re_LeftArm, Re_RightArm;
    [SerializeField] Camera_Cont cm;

    void Start()
    {
        isGravity = gameObject.GetComponent<Rigidbody>();
        Re_LeftArm = Left_Arm.transform.position;
        Re_RightArm = Right_Arm.transform.position;
    }

    public void OnDrag()
    {
        isGravity.useGravity = false;
        TapPos = Input.mousePosition;
        TapPos.y = Mathf.Clamp(TapPos.y, 170, 500);
        TapPos.z = 1.2f - Mathf.Abs(transform.position.x) / 2;
        Debug.Log(TapPos.y + "/" + Camera.main.ScreenToWorldPoint(TapPos));
        transform.position = Camera.main.ScreenToWorldPoint(TapPos);
        if (Camera.main.ScreenToWorldPoint(TapPos).x > 0)
        {
            Left_Arm.transform.position = Re_LeftArm;
            Right_Arm.transform.position = Camera.main.ScreenToWorldPoint(TapPos);
        }
        else
        {
            Right_Arm.transform.position = Re_RightArm;
            Left_Arm.transform.position = Camera.main.ScreenToWorldPoint(TapPos);
        }
    }
}
