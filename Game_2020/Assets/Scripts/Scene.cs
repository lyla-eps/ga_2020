﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scene : MonoBehaviour
{
    //　非同期動作で使用するAsyncOperation
    private AsyncOperation async;
    //　シーンロード中に表示するUI画面
    [SerializeField]
    private GameObject loadUI;
    //　読み込み率を表示するスライダー
    [SerializeField]
    private Slider slider;

    [SerializeField] GameObject Buttons;
    [SerializeField] GameObject Camera_Object,Panel;
    [SerializeField] GameObject closeWindow;
    [SerializeField] bool SetActiveBool;
    private ResultText result_text;

    private void Awake()
    {
        if(GameObject.Find("ResultScriptManagerObject") != null)
        {
            result_text = GameObject.Find("ResultScriptManagerObject").GetComponent<ResultText>();
        }
    }

    public void Go_Title()
    {
        DestroyGameClone();
        SceneManager.LoadScene(0);
    }

    public void Go_Menu()
    {
        DestroyGameClone();
        FadeManager.FadeOut(1);
    }

    public void Go_Game()
    {
        DestroyGameClone();
        loadUI.SetActive(true);

        //　コルーチンを開始
        StartCoroutine("Game_Next");
        //SceneManager.LoadScene(5);
    }

    public void Go_Rezult()
    {
        SceneManager.LoadScene(6);
    }

    public void Return_Game()
    {
        Camera_Cont camera_Cont = Camera_Object.GetComponent<Camera_Cont>();
        camera_Cont.Pose_Now = false;
        Time.timeScale = 1f;
        Panel.SetActive(false);
    }

    public void Button_SetActive()
    {
        Buttons.SetActive(SetActiveBool);
        SetActiveBool = !SetActiveBool;
    }

    public void Game_Close()
    {
        closeWindow.SetActive(true);
    }

    public void DestroyGameClone()
    {
        if(result_text != null)
        {
            result_text.DestroyGameObject();
        }
    }

    IEnumerator Game_Next()
    {
        yield return new WaitForSeconds(1f);

        switch (PlayerPrefs.GetInt("Kyaku"))
        {
            case 1:
                async = SceneManager.LoadSceneAsync("EnterCut_Easy");
                break;
            case 2:
                async = SceneManager.LoadSceneAsync("EnterCut_Normal");
                break;
            case 3:
                async = SceneManager.LoadSceneAsync("EnterCut_Hard");
                break;
        }
        

        while (!async.isDone)
        {
            var progressVal = Mathf.Clamp01(async.progress / 0.9f);
            slider.value = progressVal;
            yield return null;
        }
    }
}
