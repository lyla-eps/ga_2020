﻿using UnityEngine;

public class SetAc : MonoBehaviour
{
    [SerializeField] GameObject obj;
    [SerializeField] GameObject obj1, obj2;
    [SerializeField] GameObject text;
    int count = 0;

    public void Set()
    {
        text.GetComponent<Eat>().EatStart();
        Invoke("SSS", 3f);
    }

    void SSS()
    {
        switch (count)
        {
            case 0:
                obj.SetActive(true);
                obj1.SetActive(false);
                count++;
                break;

            case 1:
                obj.SetActive(true);
                obj2.SetActive(false);
                count++;
                break;
        }
    }
}
