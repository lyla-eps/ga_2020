﻿using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField] GameObject obj;
    static public GameObject fried;

    void Start()
    {
        fried.GetComponent<Rigidbody>().useGravity = false;
        fried.transform.parent = gameObject.transform;
        fried.transform.position = obj.transform.position;
        fried.transform.rotation = obj.transform.rotation;
        fried.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GameObject Oya = this.gameObject.transform.parent.gameObject;
        Oya.transform.parent.gameObject.GetComponent<SetAc>().Set();
        GameSetActive.dishCount++;
        Invoke("Desh", 1f);
        //ゲストにスコアを受け渡している
        Oya.transform.parent.gameObject.GetComponent<Guest>().ScoreSet(fried.GetComponent<FriedTiming>().score);
    }

    void Desh()
    {
        Destroy(gameObject);
    }
}
