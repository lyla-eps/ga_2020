﻿using System.Collections;
using UnityEngine;

public class TitleManager : MonoBehaviour
{
    [SerializeField] AudioSource bgm_source,se_source;
    [SerializeField] GameObject ClegitOya, ClegitKo;
    int volume = 0;

    //--------------------------------------------------------------------------------------
    [SerializeField] GameObject _Start_big, _start_small;   //スタートボタンの動き用
    //--------------------------------------------------------------------------------------

    //フェードアウトさせてそのとき音もだんだん小さくさせる

    public void Start()
    {
        PlayerPrefs.SetInt("Tyuto", 0);
    }

    public void Open_Clegit()
    {
        ClegitKo.SetActive(true);
        ClegitOya.SetActive(false);
    }

    public void Close_Clegit()
    {
        ClegitKo.SetActive(false);
        ClegitOya.SetActive(true);
    }

    public void Menu_Idou()
    {
        //--------------------------------------------------------------------------------------
        _start_small.SetActive(false);
        _Start_big.SetActive(true);
        //--------------------------------------------------------------------------------------
        se_source.Play();
        StartCoroutine("Go_Menu");
    }

    //--------------------------------------------------------------------------------------
    public void Start_Motion_Enter()
    {
        _Start_big.SetActive(false);
        _start_small.SetActive(true);
        Debug.Log("あくしょんするやよ～");
    }
    public void Star_Motion_Exit()
    {
        _start_small.SetActive(false);
        _Start_big.SetActive(true);
        Debug.Log("あくしょんやめたやよ～");
    }
    //--------------------------------------------------------------------------------------

    IEnumerator Go_Menu()
    {
        for (; volume > 0; volume--)
        {
            bgm_source.volume = (float)(volume) / 100;
            yield return null;
        }
        bgm_source.volume = 0;
        FadeManager.FadeOut(8);
        yield return null;
    }
}