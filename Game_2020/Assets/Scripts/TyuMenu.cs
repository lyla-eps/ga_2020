﻿using UnityEngine;

public class TyuMenu : MonoBehaviour
{
    [SerializeField] GameObject Cus, Nos;
    [SerializeField] TyutorialMenu TyutorialMenu;

    bool opencheck = true;
    bool textSet = false;
    bool textcheck = true;

    [SerializeField] GameObject Hensei_Normal, Hensei_Highlight;
    public bool hilightcheck = false;

    public void Open()
    {
        if(TyutorialMenu.open == true)
        {

            if (textcheck == true)
            {
                hilightcheck = true;
            }

            Cus.SetActive(opencheck);
            if (opencheck == true)
            {
                TyutorialMenu.SetCol();
            }
            else
            {
                hilightcheck = false;
                TyutorialMenu.EndTyu();
            }

            opencheck = !opencheck;
            if (!textSet)
            {
                Debug.Log("close");
            }
            textSet = true;
        }
    }

    public void HilightChenge()
    {
        Hensei_Highlight.SetActive(false);
        Hensei_Normal.SetActive(true);
    }

    public void HighlihgtOn()
    {
        Hensei_Normal.SetActive(false);
        Hensei_Highlight.SetActive(true);
    }

    public void HightlightOff()
    {
        if (hilightcheck == false)
        {
            Hensei_Highlight.SetActive(false);
            Hensei_Normal.SetActive(true);
        }
    }
}
