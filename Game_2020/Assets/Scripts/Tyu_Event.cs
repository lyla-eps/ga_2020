﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tyu_Event : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] int CameraPotion_Namber;
    [SerializeField] Camera_Cont cm;
    [SerializeField] LeftOut LeftOut;
    [SerializeField] MidolOut MidolOut;
    [SerializeField] RightOut RightOut;
    [SerializeField] TyutorialManager tyutorial;

    int check_num = 0;
    public void OnPointerDown(PointerEventData eventData)
    {
        if(tyutorial.Nabe_Check == true)
        {
            switch (CameraPotion_Namber)
            {
                case 1:
                    if(tyutorial.camera_check == false)
                    {
                        tyutorial.Nabe_Check = false;
                        tyutorial.OileCheckWait();
                    }
                    cm.LeftCamera();
                    break;
                case 2:
                    if (tyutorial.camera_check == false)
                    {
                        tyutorial.Nabe_Check = false;
                        tyutorial.OileCheckWait();
                    }
                    cm.ManakaCamera();
                    break;
                case 3:
                    if (tyutorial.camera_check == false)
                    {
                        tyutorial.Nabe_Check = false;
                        tyutorial.OileCheckWait();
                    }
                    cm.RightCamera();
                    break;
                case 4:
                    if(tyutorial.camera_check == true && check_num == 0)
                    {
                        check_num++;
                        tyutorial.DishMoveText();
                    }
                    cm.ReturnCamera();
                    break;
            }
        }
    }

    public void LeftTrue()
    {
        LeftOut.enabled = true;
    }

    public void LeftFalse()
    {
        LeftOut.enabled = false;
    }

    public void MidolTrue()
    {
        MidolOut.enabled = true;
    }

    public void MIdolFalse()
    {
        MidolOut.enabled = false;
    }

    public void RightTrue()
    {
        RightOut.enabled = true;
    }

    public void RightFalse()
    {
        RightOut.enabled = false;
    }
}
