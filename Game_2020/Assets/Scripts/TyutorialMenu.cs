﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TyutorialMenu : MonoBehaviour
{
    //　非同期動作で使用するAsyncOperation
    private AsyncOperation async;
    //　シーンロード中に表示するUI画面
    [SerializeField]
    private GameObject loadUI;
    //　読み込み率を表示するスライダー
    [SerializeField]
    private Slider slider;

    string menu1 = "とんかつ", menu2 = "アジフライ";
    [SerializeField] Text Menu_Text1, Kettei_Text;

    int m_1;

    [SerializeField] GameObject Kettei_Obj, No_Obj, Bay_Syou, Bay_Dai;

    int Ichiban_Syoku_No = 0, Niban_Syoku_No = 0, Sanban_Syoku_No = 0, Kyaku_Ninzu = 0;

    public int Custm_Ichi_Syoku_No = 0, Custm_Niban_Syoku_No = 0, Custm_San_Syoku_No = 0;

    [SerializeField] AudioSource bgm, se_kettei, se_re;

    int volume = 0;

    //------------------------------------------------------------------------------------------------
    [SerializeField] GameObject menu_induction_text, menu_induciton_text1, menu_induction_text2, menu_induction_text3, menu_induction_text4, menu_induction_text5;  //メニューの操作誘導用テキスト
    bool guidance_decision = false;
    //------------------------------------------------------------------------------------------------

    [SerializeField] Animator _Animator;

    [SerializeField] TyuMenu menu;

    public bool firstcheck = false, secondcheck = false, thirdcheck = false;

    public bool open = false;

    public int checkc = 0;

    [SerializeField] GameObject jama;

    [SerializeField] public float Word_Speed = 2.0f;

    private void Awake()
    {
        PlayerPrefs.SetInt("Gage", 0);

        //1人用ステージ決定
        m_1 = Random.Range(0, 2);

        StartCoroutine("One_Game");
    }

    //チュートリアル
    IEnumerator One_Game()
    {
        menu.enabled = false;

        Rest(true, false, false);
        if (m_1 == 0)
        {
            Menu_Text1.text = $"壱番席ー{menu1}";
            Ichiban_Syoku_No = 1;
        }
        else
        {
            Menu_Text1.text = $"壱番席ー{menu2}";
            Ichiban_Syoku_No = 2;
        }
        Niban_Syoku_No = 0;
        Sanban_Syoku_No = 0;

        yield return new WaitForSeconds(Word_Speed);

        menu_induction_text.SetActive(false);
        menu_induciton_text1.SetActive(true);
        //Animator.SetBool("Check", true);
        _Animator.Play("serifu");

        yield return new WaitForSeconds(Word_Speed);

        menu_induciton_text1.SetActive(false);
        menu_induction_text2.SetActive(true);
        menu.enabled = true;
        open = true;
    }

    public void SetCol()
    {
        open = false;
        menu_induction_text2.SetActive(false);
        menu_induction_text3.SetActive(true);
    }

    public void SetOff()
    {
        if(checkc >= 3)
        {
            open = true;
            menu_induction_text3.SetActive(false);
            menu_induction_text4.SetActive(true);
            //menu.HilightChenge();
        }
    }


    public void EndTyu()
    {
        jama.SetActive(false);
        menu_induction_text4.SetActive(false);
        menu_induction_text5.SetActive(true);
        open = false;
    }

    public void Rest(bool Syou, bool Tiku, bool Bai)
    {
        Menu_Text1.text = $"";
        Bay_Syou.SetActive(!Bai);
        Bay_Dai.SetActive(Bai);
    }

    public void One_Chenge()
    {
        Kettei_Text.text = "揚げ物好きおじいさん(来店人数1人)";
        Kyaku_Ninzu = 1;
        K_True();
    }

    public void K_True()
    {
        if (Custm_Ichi_Syoku_No == 0 || Custm_Niban_Syoku_No == 0 || Custm_San_Syoku_No == 0)
        {
            No_Obj.SetActive(true);

            //-----------------------------------------
            menu_induction_text2.SetActive(false);
            menu_induction_text.SetActive(false);
            if (!guidance_decision)
            {
                menu_induciton_text1.SetActive(true);
                guidance_decision = true;
            }
            //-----------------------------------------
        }
        else
        {
            Kettei_Obj.SetActive(true);
        }
    }

    public void K_False()
    {
        se_re.Play();
        Kettei_Obj.SetActive(false);
    }

    public void GoTyutolial()
    {
        PlayerPrefs.SetInt("WantSeki1", Ichiban_Syoku_No);
        PlayerPrefs.SetInt("WantSeki2", Niban_Syoku_No);
        PlayerPrefs.SetInt("WantSeki3", Sanban_Syoku_No);
        PlayerPrefs.SetInt("Syoku1", Custm_Ichi_Syoku_No);
        PlayerPrefs.SetInt("Syoku2", Custm_Niban_Syoku_No);
        PlayerPrefs.SetInt("Syoku3", Custm_San_Syoku_No);
        PlayerPrefs.SetInt("Kyaku", Kyaku_Ninzu);

        loadUI.SetActive(true);
        StartCoroutine("Tyutolial_Next");
    }
}
