﻿using UnityEngine;

public class Wait : MonoBehaviour
{
    [SerializeField] GameObject  obj2, obj3;
    [SerializeField] Camera_Cont Camera_Cont;
    [SerializeField] TyutorialManager TyutorialManager;

    private void Awake()
    {
        Invoke("Next",2f);
    }

    public void Next()
    {
        obj2.SetActive(true);
        obj3.SetActive(true);
        Camera_Cont.Rotation(30, 0, 1);
        this.gameObject.SetActive(false);
        if(TyutorialManager != null)
        {
            TyutorialManager.enabled = true;
        }
    }
}
