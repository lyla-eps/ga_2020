﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TyutorialManager : MonoBehaviour
{
    [SerializeField] GameObject Background,DefaultText,FirstText, SecondText,ThirdText,YonText,FiveText,SixText,sevenText,eightText;
    [SerializeField] GameObject AmiFirstText, AmiSecondText, AmiThirdText;
    [SerializeField] TolerRight TolerRight;
    [SerializeField] AmiLight amiLight;
    [SerializeField] AmiDrag AmiDrag;
    [SerializeField] GameObject dish;
    private bool setactive = true, DishMoveCheck = true,  DishColiderCheck = true;
    public bool Right_Check = false;
    BoxCollider BoxCollider;

    [SerializeField] public float Word_Speed = 2.0f;

    [SerializeField] MeshCollider Oile1, Oile2, Oile3;

    public bool Nabe_Check = false, camera_check = false;

    [SerializeField] GameObject Jamar;

    private void Start()
    {
        BoxCollider = dish.GetComponent<BoxCollider>();
        BoxCollider.enabled = false;
        StartCoroutine("TyuToLial");
    }

    IEnumerator TyuToLial()
    {
        Image image = Background.GetComponent<Image>();
        image.raycastTarget = true;
        Background.SetActive(true);
        DefaultText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        DefaultText.SetActive(false);
        image.raycastTarget = false;
        StartCoroutine("Ami");
    }

    IEnumerator Ami()
    {
        YonText.SetActive(false);
        amiLight.enabled = true;
        AmiFirstText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        AmiFirstText.SetActive(false);
        AmiSecondText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        AmiSecondText.SetActive(false);
        AmiThirdText.SetActive(true);
        AmiDrag.enabled = true;
        StartCoroutine("Dropp");
    }

    IEnumerator Dropp()
    {
        Jamar.SetActive(false);
        yield return new WaitForSeconds(Word_Speed);
        AmiThirdText.SetActive(false);
        FirstText.SetActive(true);
        while (Right_Check == false)
        {
            TolerRight.enabled = setactive;
            setactive = !setactive;
            yield return new WaitForSeconds(Word_Speed);
        }
    }

    public void DropTrueText()
    {
        if(DishMoveCheck == true)
        {
            FirstText.SetActive(false);
            SecondText.SetActive(true);
            Oile1.enabled = true;
            Oile2.enabled = true;
            Oile3.enabled = true;
        }
    }

    public void DropText()
    {
        if(DishMoveCheck == true)
        {
            SecondText.SetActive(false);
            ThirdText.SetActive(true);
            Nabe_Check = true;
            //StartCoroutine("Ami");
        }
        DishMoveCheck = false;
    }


    public void OileCheckWait()
    {
        StartCoroutine("OIleText");
    }

    IEnumerator OIleText()
    {
        ThirdText.SetActive(false);
        YonText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        Nabe_Check = true;
        camera_check = true;
    }

    public void AmiWait()
    {
        //StartCoroutine("Ami");
    }


    public void DishMoveText()
    {
        BoxCollider.enabled = true;
        if (DishColiderCheck == true)
        {
            YonText.SetActive(false);
            FiveText.SetActive(true);
            DishColiderCheck = false;
        }
    }

    public void DishNext()
    {
        StartCoroutine("Gamenext");
    }

    IEnumerator Gamenext()
    {
        FiveText.SetActive(false);
        SixText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        SixText.SetActive(false);
        sevenText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        sevenText.SetActive(false);
        eightText.SetActive(true);
        yield return new WaitForSeconds(Word_Speed);
        eightText.SetActive(false);
        Background.SetActive(false);
    }
}
