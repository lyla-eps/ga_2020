# 日本ゲーム大賞2020 「アマチュア部門」 に応募した作品となっています。
操作説明PDF https://gitlab.com/lyla-eps/ga_2020/-/blob/develop/%E6%8F%9A%E3%81%92%E5%A5%89%E8%A1%8C_%E6%93%8D%E4%BD%9C%E8%AA%AC%E6%98%8E.pdf  

ゲーム紹介PDF https://gitlab.com/lyla-eps/ga_2020/-/blob/develop/%E6%8F%9A%E3%81%92%E5%A5%89%E8%A1%8C%E3%82%B2%E3%83%BC%E3%83%A0%E7%B4%B9%E4%BB%8B.pdf  

ゲーム紹介動画はこちらから→ https://youtu.be/VRwS3clFd5Y